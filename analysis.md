# Scrum Tool Requirements Analysis

Από την ομάδα team_rocket

## I. Business Analysis

### 1. Functional Requirements

1. User management
    1. Register new user account
    2. Verify email address
    3. Sign in & Sign out
    4. Recover user account on forgotten password
    5. Edit user profile details
    6. Delete user account (GDPR)

2. Customer functionality
    1. Create new project
    2. Invite user to project
    3. Remove user from project
    4. Edit existing project
    5. Delete existing project
    6. Add billing details
    7. Edit billing details

3. Admin user functionality
    1. Create new sprint
    2. Edit existing sprint
    3. Start / End sprint

4. Team & Project functionality
    1. Include backlog on team/project creation
    2. View team/project homepage
        1. Display description
        2. Display members & admins
        3. Display link to backlog
        4. Display links to current and past sprints

5. Backlog functionality
    1. List all backlog user stories
    2. Add new user story to backlog
    3. Delete existing user story from backlog
    4. Sort user stories in backlog list

6. Sprints functionality
    1. Display To Do, Doing and Done columns
    2. Display user stories in columns based on user story state
    3. Add user stories from backlog to sprint
    4. Remove user stories from sprint
    5. Move user stories between columns
    6. Filter stories shown per user
    7. (Team admin only) Edit sprint start & end date, sprint name & description
    8. (Team admin only) Start & End sprint

7. User stories functionality
    1. Create user story
    2. Assign user story to backlog or sprint
    3. Assign user story to user
    4. Edit user story details
    5. Delete user story
    6. Set user story state (Todo, Doing, Done) in sprint
    7. Assign story to one or more epics
    8. User Story Tasks functionality
        1. Create new task under user story
        2. Edit task details
        3. Delete task

8. Epics functionality
    1. Create new epic in project
    2. Edit epic's details
    3. List epic's user stories

9. Homepage functionality
    1. Logged in users
        1. Display user's project
        2. Display user's running sprints per project
        3. Display user's "doing" and "todo" user stories per sprint
        4. Link to list of user's "done" user stories
        5. Show user's newsfeed (TBD
    2. Logged out users
        1. Display Sign up form
        2. Display Link to registration form

10. Notifications functionality
    1. Notify users in the case of the following events
        1. A sprint they participate in has started
        2. A sprint they participate in has ended
        3. They were invited to join a team
        4. They were removed from a team
    2. Notify customers in the following events
        1. Invoice generated - payment is due
        2. Payment was successfull
        3. Payment failed
    3. Support notifications with the following means
        1. Email
        2. In app notification


### 2. Non-Functional Requirements
1. Perform decently under normal user load
2. Allow multiple hundreds of users and teams to use the service
3. Be available and always online (99.0% uptime)
4. Have average response times lower than 2s
5. Follow modern password requirements
6. Encrypt data transmitted between server and clients
7. Secure against OWASP's Top 10 Most Critical Web Application Security Risks
8. Operate on the cloud
9. Achieve high test coverage
10. Document software architecture and technical decisions

### 3. UML Use Cases

- **Main (High level)**
    ![](https://gitlab.com/eugenetsi/TL--team_rocket/raw/master/uml%20/MAIN.jpg)
- **Account expansion**
    ![](https://gitlab.com/eugenetsi/TL--team_rocket/raw/master/uml%20/manage-account-expansion.jpg)
- **Backlog expansion**
    ![](https://gitlab.com/eugenetsi/TL--team_rocket/raw/master/uml%20/manage-backlog-expansion.jpg)
- **Customer expansion**
    ![](https://gitlab.com/eugenetsi/TL--team_rocket/raw/master/uml%20/Customer%20Cases.%20expansionpng.png)
- **Sprint expansion**
    ![](https://gitlab.com/eugenetsi/TL--team_rocket/raw/master/uml%20/manage-sprint-expansion.jpg)
- **Issues expansion**
    ![](https://gitlab.com/eug
enetsi/TL--team_rocket/raw/master/uml%20/issues-expansion.png)

### 4. User Interface Wireframes

Please see folder https://gitlab.com/eugenetsi/TL--team_rocket/tree/master/Wireframes

## II. Technical & Implementation Analysis

### 1. Entities - Relationships Model

Please see folder https://gitlab.com/eugenetsi/TL--team_rocket/tree/master/db

A screenshot of the model is included.

### 2. Restful API Description

1. URL map
    1. https://scrumtool.com/
    2. https://scrumtool.com/login
    3. https://scrumtool.com/register
    4. https://scrumtool.com/index
    5. https://scrumtool.com/profile
    6. https://scrumtool.com/profile/**ID**
    7. https://scrumtool.com/profile/settings
    8. https://scrumtool.com/create_project
    9. https://scrumtool.com/projects/overview
    10. https://scrumtool.com/projects/overview/**ID**
    11. https://scrumtool.com/projects/backlog
    12. https://scrumtool.com/projects/sprint
    13. https://scrumtool.com/projects/sprint/current
    14. https://scrumtool.com/projects/sprint/previous
    15. https://scrumtool.com/projects/epic
    16. https://scrumtool.com/projects/issues
    17. https://scrumtool.com/teams
    18. https://scrumtool.com/create_team
    19. https://scrumtool.com/stories

2. URL Endpoints
    3. https://scrumtool.com/index/**#current**
    3. https://scrumtool.com/index/**#finished**
    4. https://scrumtool.com/index/**#profile**
    5. https://scrumtool.com/index/**#profile**/**ID**
    6. https://scrumtool.com/index/**#profile**/**#settings**
    7. https://scrumtool.com/index/**#projects**
    8. https://scrumtool.com/index/**#projects**/**#overview**
    9. https://scrumtool.com/index/**#projects**/**#overview**/**ID**
    10. https://scrumtool.com/index/**#projects**/**#backlog**
    11. https://scrumtool.com/index/**#projects**/**#sprint**
    12. https://scrumtool.com/index/**#projects**/**#sprint**/**#current**
    13. https://scrumtool.com/index/**#projects**/**#sprint**/**#previous**
    14. https://scrumtool.com/index/**#projects**/**#epic**
    15. https://scrumtool.com/index/**#projects**/**#issues**
    16. https://scrumtool.com/index/**#teams**
    17. https://scrumtool.com/index/**#teams**/**#create**

3. HTTPS Methods (for each endpoint)
    2. https://scrumtool.com/login/login
        - C: POST /login HTTPS/1.1 + JSON object type of User
        - S: HTTPS/1.1 201 OK 
    3. https://scrumtool.com/register
        - C: POST /register HTTPS/1.1 + JSON object type of User
        - S: HTTPS/1.1 201 OK
    4. https://scrumtool.com/profile
        - C: GET /profile HTTP/1.1
        - S: HTTPS/1.1 200 OK + JSON object type of User_Profile for specific User.ID  
    5. https://scrumtool.com/profile/**ID**
        - C: GET /profile/edit HTTPS/1.1
        - S: HTTPS/1.1 200 OK + JSON object type of User_Profile for specific User.ID
    6. Etc. (all others are similar GET)

4. Endpoint parameters
    1. URL parameters
        - 1. https://scrumtool.com/projects
            - C: GET /projects?userid=1&blabla=2
            - S: HTTPS/1.1 200 OK + JSON object

    2. Request body parameters
        - 1. https://scrumtool.com/projects
            - C: POST /projects
                Content-Type: application/json
                userid=1&blabla=2
            - S: HTTPS/1.1 201 OK
        - 2. https://scrumtool.com/login
            - C: POST /login
                Content-Type: application/json
                username=1&psw=2
            - S: HTTPS/1.1 201 OK

5. JSON Payload
    1. Resource Payload Type
        - User Resource, Sign up

    ```
        [
        {"name": "blabla", 
        "username": "test", 
        "email": "test@example.org", 
        "password": "abc12345"}
        ]
    ```

        - User Resource, Sign in

    ```
        [
        {"username": "test", 
        "password": "abc12345"}
        ]
    ```

    2. Search Payload Type 

    3. List Payload Type

    ```
        [
            { "href": "https://scrumtool.com/foo/1" },
            { "href": "https://scrumtool.com/foo/2" },
            { "href": "https://scrumtool.com/foo/3" }
        ]
    ```
        OR
    ```
        [
            {
                "href": "https://scrumtool.com/foo/1",
                    "value": {
                        "code": "FOO1",
                        ...
                    }
            },
            {
                "href": "https://scrumtool.com/foo/2",
                "value": {
                    "code": "FOO2",
                    ...
                }
            },
            {
                "href": "https://scrumtool.com/foo/3",
                "value": {
                    "code": "FOO3",
                    ...
                }
            },
    ]
    ```

6. Authentication tokens
    - In this application, JWTs are used for authentication.
    The biggest advantage of JWTs (when compared to user session management using an in-memory random token) is that they enable the delegation of the authentication logic to a third-party server. The application server can be completely stateless, as there is no need to keep tokens in-memory between requests. The authentication server can issue the token, send it back and then immediately discard it. Also, there is also no need to store password digests at the level of the application database either, so fewer things to get stolen and less security-related bugs.
    JWTs are digitally signed JSON payloads, encoded in a URL-friendly string format. A JWT can contain any payload in general, but the most common use case is to use the payload to define a user session. The key thing about JWTs is that in order to confirm if they are valid, we only need to inspect the token itself and validate the signature, without having to contact a separate server for that, or keeping the tokens in memory or in the database between requests. Our JWTs will contain at least a user ID and an expiration timestamp. 
    After signing up, the client sends the user data encrupted on https with a json object. The server encrypts and stores that data on the db. A JWT (JSON Web Token) is then sent to the client, containing an expiration timestamp and the user ID, which are used to define the user session. Each time the client communicates with the server, that valid token must be also present in the http request body.
