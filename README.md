The app runs with the command ` ./gradlew apprun `
in the directory scrumtool and loads to localhost:8765 .

Two tabs then appear, from which the user can select from,
sign in and sign up. In signing up the user is required to
fill the following fields; username, password, email, full name.
If one of the fields is not filled in, the user can not 
successfully sign up. When the user completes the process
a success or failure (if i.e. the username is taken) 
is displayed. Then the user can sign in, using the 
aforementioned credentials. If the credentials are not valid 
an error messege is displayed. If they are valid, the user 
is redirected to the homepage (not functional yet).


DB CREDENTIALS:
database username: root
database pwd: scrum
database name: scrumtool
