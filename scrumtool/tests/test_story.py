from urllib.parse import urljoin


def test_story(base_url, logged_in_client):
    user, client = logged_in_client
    user_url = urljoin(base_url, 'users/{}'.format(user['id']))
    # Create new story
    url = urljoin(base_url, 'stories')
    payload = {
        'name': 'test name',
        'description': 'test description',
        'user': user_url,
    }
    resp = client.post(url, json=payload)
    assert resp.status_code == 201
    assert resp.json()['name'] == payload['name']
    assert resp.json()['description'] == payload['description']
    assert resp.json()['status'] == 'TODO'
    url = resp.json()['_links']['self']['href']
    # Update story
    payload = {
        'name': 'update test name',
        'status': 'DONE',
    }
    resp = client.put(url, json=payload)
    assert resp.status_code == 200
    assert resp.json()['name'] == payload['name']
    assert resp.json()['status'] == payload['status']
    assert resp.json()['_links']['self']['href'] == url

    # Create new epic
    create_epic_url = urljoin(base_url, 'epics')
    payload = {
        'name': 'test epic',
        'description': 'test description',
    }
    resp = client.post(create_epic_url, json=payload)
    assert resp.status_code == 201
    epic_url = resp.json()['_links']['self']['href']
    # Add epic to story
    payload = {
        'epics': [epic_url],
    }
    resp = client.patch(url, json=payload)
    assert resp.status_code == 200
    # Delete story
    resp = client.delete(url)
    assert resp.status_code == 204
