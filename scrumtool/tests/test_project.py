from urllib.parse import urljoin


def test_project(base_url, logged_in_client):
    user, client = logged_in_client
    # Create new
    url = urljoin(base_url, 'projects')
    payload = {
        'name': 'test name',
        'description': 'test description',
        'users': [user]
    }
    resp = client.post(url, json=payload)
    assert resp.status_code == 201
    assert resp.json()['name'] == payload['name']
    assert resp.json()['description'] == payload['description']
    id_ = resp.json()['id']
    # Update
    payload = {
        'id': resp.json()['id'],
        'name': 'update test name',
        'users': [user]
    }
    resp = client.put(url, json=payload)
    assert resp.status_code == 200
    assert resp.json()['name'] == payload['name']
    # Delete epic
    resp = client.delete(f'{url}/{id_}')
    assert resp.status_code == 200


def test_project_users(base_url, logged_in_client, another_logged_in_client):
    user, client = logged_in_client
    user2, client2 = another_logged_in_client
    # Create new project
    url = urljoin(base_url, 'projects')
    payload = {
        'name': 'test name 2',
        'description': 'test description',
        'users': [user]
    }
    resp = client.post(url, json=payload)
    assert resp.status_code == 201
    assert len(resp.json()['users']) == 1
    assert resp.json()['users'][0]['id'] == user['id']
    id_ = resp.json()['id']
    resp = client.get(f'{url}?usersId.equals={user["id"]}')
    assert len(resp.json()) == 1
    # Show only projects of user
    resp = client2.get(url)
    assert len(resp.json()) == 0

    resp = client.get(f'{url}/{id_}')
    assert resp.json()['id'] == id_

    resp = client2.get(f'{url}/{id_}')
    assert resp.status_code == 404
    # Delete
    resp = client2.delete(f'{url}/{id_}')
    assert resp.status_code == 404

    resp = client.delete(f'{url}/{id_}')
    assert resp.status_code == 200
