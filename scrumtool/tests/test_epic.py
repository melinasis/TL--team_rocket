from urllib.parse import urljoin


def test_epic(base_url, logged_in_client):
    user, client = logged_in_client
    # Create new epic
    url = urljoin(base_url, 'epics')
    payload = {
        'name': 'test name',
        'description': 'test description',
    }
    resp = client.post(url, json=payload)
    assert resp.status_code == 201
    assert resp.json()['name'] == payload['name']
    assert resp.json()['description'] == payload['description']
    id_ = resp.json()['id']
    # Update epic
    payload = {
        'id': id_,
        'name': 'update test name',
    }
    resp = client.put(url, json=payload)
    assert resp.status_code == 200
    assert resp.json()['name'] == payload['name']
    # Delete epic
    resp = client.delete(f'{url}/{id_}')
    assert resp.status_code == 200
