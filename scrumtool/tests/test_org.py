from urllib.parse import urljoin

import requests


def test_post_org(base_url, logged_in_client):
    user, client = logged_in_client
    url = urljoin(base_url, 'organizations')
    payload = {
        'name': 'orgname',
    }
    resp = client.post(url, json=payload)
    assert resp.status_code == 201
    assert resp.json()['name'] == 'orgname'


def test_post_org_unauthorized(base_url):
    url = urljoin(base_url, 'organizations')
    payload = {'name': 'orgname'}
    resp = requests.post(url, json=payload)
    assert resp.status_code == 401
