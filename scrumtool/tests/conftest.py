import random
import string
from urllib.parse import urljoin

import pytest
import requests


def pytest_addoption(parser):
    parser.addoption("--url", action="store", help="Backend API base url",
                     default="http://dockervm:8080/api/")


@pytest.fixture
def base_url(request):
    return request.config.getoption("--url")


def create_client(base_url):
    s = requests.Session()
    rnd = ''.join(random.choices(
        string.ascii_uppercase + string.digits, k=4))
    u, p = f'username-{rnd}', 'password'
    payload = {
        'login': u,
        'password': p,
        'firstName': 'John',
        'lastName': 'Doe',
        'email': f'{u}@example.com',
    }

    url = urljoin(base_url, 'register')
    resp = requests.post(url, json=payload)
    assert resp.status_code == 201

    # Login
    url = urljoin(base_url, 'authenticate')
    payload = {'username': u, 'password': p}
    resp = s.post(url, json=payload)
    assert resp.status_code == 200
    assert resp.headers.get('Authorization', '').startswith('Bearer ')
    s.headers['Authorization'] = resp.headers.get('Authorization')

    url = urljoin(base_url, 'account')
    resp = requests.get(url, headers=resp.headers)
    assert resp.status_code == 200
    return resp.json(), s


@pytest.fixture
def logged_in_client(base_url):
    return create_client(base_url)


@pytest.fixture
def another_logged_in_client(base_url):
    return create_client(base_url)
