import random
import string
from urllib.parse import urljoin

import requests

rnd = ''.join(random.choices(
    string.ascii_uppercase + string.digits, k=4))
u, p = f'username-{rnd}', 'password'


def test_post_user(base_url):
    url = urljoin(base_url, 'users')
    payload = {
        'username': u,
        'password': p,
        'name': 'name',
        'email': f'{u}@example.com',
    }
    resp = requests.post(url, json=payload)
    assert resp.status_code == 200
    expected = payload
    expected['userProjects'] = None
    expected.pop('password', None)
    resp = resp.json()
    resp.pop('id')
    assert resp == expected

    # User exists
    resp = requests.post(url, json=payload)
    assert resp.status_code == 400


def test_post_user_incomplete(base_url):
    url = urljoin(base_url, 'users')
    payload = {
        'username': 'username',
        'password': 'password',
    }
    resp = requests.post(url, json=payload)
    assert resp.status_code == 400


def test_login(base_url):
    url = urljoin(base_url, 'login')
    payload = {
        'username': u,
        'password': p,
    }
    resp = requests.post(url, json=payload)
    assert resp.status_code == 200
    assert resp.headers.get('Authorization', '').startswith('Bearer ')

    url = urljoin(base_url, 'users/me')
    resp = requests.get(url, headers=resp.headers)
    assert resp.status_code == 200
    assert resp.json()['username'] == u
    assert resp.json()['userProjects'] == []
    assert 'id' in resp.json()


def test_get_users(base_url, logged_in_client):
    url = urljoin(base_url, 'users')
    user, client = logged_in_client
    resp = client.get(url)
    assert resp.status_code == 200
