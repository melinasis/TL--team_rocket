import random
import string
from urllib.parse import urljoin

import requests


rnd = ''.join(random.choices(
    string.ascii_uppercase + string.digits, k=4))
u, p = f'username-{rnd}', 'password'


def test_register(base_url):
    url = urljoin(base_url, 'register')
    payload = {
        'login': u,
        'password': p,
        'firstName': 'John',
        'lastName': 'Doe',
        'email': f'{u}@example.com',
    }
    resp = requests.post(url, json=payload)
    assert resp.status_code == 201


def test_login(base_url):
    url = urljoin(base_url, 'authenticate')
    payload = {
        'username': u,
        'password': p,
    }
    resp = requests.post(url, json=payload)
    assert resp.status_code == 200
    assert resp.headers.get('Authorization', '').startswith('Bearer ')

    url = urljoin(base_url, 'account')
    resp = requests.get(url, headers=resp.headers)
    assert resp.status_code == 200
    assert 'id' in resp.json()
