from urllib.parse import urljoin


def test_sprint(base_url, logged_in_client):
    user, client = logged_in_client
    # Create new
    url = urljoin(base_url, 'sprints')
    payload = {
        'name': 'test name',
        'startDate': '2018-05-31T14:04:02.575+0000',
    }
    resp = client.post(url, json=payload)
    assert resp.status_code == 201
    assert resp.json()['name'] == payload['name']
    assert resp.json()['startDate'] == payload['startDate']
    assert resp.json()['endDate'] is None
    assert resp.json()['isBacklog'] is False
    url = resp.json()['_links']['self']['href']
    # Update
    payload = {
        'isBacklog': True
    }
    resp = client.patch(url, json=payload)
    assert resp.json()['isBacklog'] is True
    # Delete
    resp = client.delete(url)
    assert resp.status_code == 204
