﻿import { Component, OnInit } from '@angular/core';


import { User } from '../../_models/index';
import { Project } from '../../_models/index';
import { UserService } from '../../_services/index';
import { ProjectService } from '../../_services/index';

@Component({
    moduleId: module.id.toString(),
    templateUrl: './project.component.html',
    styleUrls: ['./project.component.css']
})

export class ProjectComponent implements OnInit {
    currentUser: User;
    currentProject: Project;
    devteam = [];

    constructor(private userService: UserService ,private projectService: ProjectService ) {
    	this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
      this.currentProject = JSON.parse(localStorage.getItem('currentProject'));
    }

    ngOnInit() {
        this.loadAllProjects();
    }

    private loadAllProjects() {
        //this.projectService.getAll().subscribe(projects => { this.projects = projects; });
    }

}
