import { Component, OnInit, Inject } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {MatChipsModule} from '@angular/material/chips';
import { User, Project, Epic } from '../../_models/index';
import { Router } from '@angular/router';
import { UserService, ProjectService, AlertService, EpicService} from '../../_services/index';


@Component({
  selector: module.id.toString(),
  templateUrl: './epics.component.html',
  styleUrls: ['./epics.component.css']
})

export class EpicsComponent implements OnInit {
    currentUser: User;
    name: string;
    description: string;
    epic: Epic;
    epicsList : Epic[] = [];
    loading = false;
    model: any = {};
    currentProject: Project;

    constructor(
      private userService: UserService,
      private projectService: ProjectService,
      private epicService: EpicService,
      private alertService: AlertService,
      private router: Router,
      public dialog: MatDialog
    ) {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.currentProject = JSON.parse(localStorage.getItem('currentProject'));
    }

    ngOnInit() {
      this.loadAllEpics();
    }

    private loadAllEpics() {
        this.epicService.getAll(this.currentProject.id).subscribe(epicsList => { this.epicsList = epicsList; });
    }

    openDialog(): void {
      let dialogRef = this.dialog.open(EpicsDialogComponent, {
        width: '400px',
        data: { name: this.name, description: this.description}
      });

      dialogRef.afterClosed().subscribe(result => {
        console.log('Add epic requested');

        this.name = result.name;
        this.description = result.description;

        this.epic = new Epic;
        this.epic.name = this.name;
        this.epic.description = this.description;
        this.epicsList.push(this.epic);


        //post
        this.loading = true;
        this.model.name = this.name;
        this.model.description = this.description;
        let project: Project = this.currentProject;
        delete project.users;
        delete project.sprints;
        this.model.project = this.currentProject;

        this.epicService.create(this.model)
            .subscribe(
                data => {
                    this.alertService.success('Creation of epic successful', true);
                },
                error => {
                    console.log(error);
                    this.alertService.error('Creation of epic failed!', false);
                    this.loading = false;
                });
      });

      this.name = '';
      this.description = '';
    }


    openDeleteDialog(i): void {
      let dialogRef_delete = this.dialog.open(EpicsDeleteDialogComponent, {
        width: '400px'
      });

      dialogRef_delete.afterClosed().subscribe(result => {
        // console.log(result);
        if (result == false) {
          console.log("canceled");
        }
        else{
          console.log("removed");
          this.epicsList.splice(i, 1);
        }
      });
    }
}

@Component({
  moduleId: module.id.toString(),
  selector: 'epics.dialog.component',
  templateUrl: 'epics.dialog.component.html',
  styleUrls: ['epics.component.css']
})

export class EpicsDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<EpicsDialogComponent>,
    private userService: UserService,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close();
    console.log('Add epic cancelled');
  }
}

@Component({
  moduleId: module.id.toString(),
  selector: 'epics.delete.dialog.component',
  templateUrl: 'epics.delete.dialog.component.html',
  styleUrls: ['epics.component.css']
})

export class EpicsDeleteDialogComponent {
  users: User[] = [];

  constructor(
    public dialogRef_delete: MatDialogRef<EpicsDeleteDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
  }
}
