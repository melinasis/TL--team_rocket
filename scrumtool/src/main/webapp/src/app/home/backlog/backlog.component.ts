import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import { Story, User, Project, Sprint, Epic } from '../../_models/index';
import {MatChipsModule} from '@angular/material/chips';
import { UserService, AlertService, StoryService, EpicService } from '../../_services/index';
import { Router } from '@angular/router';


@Component({
    moduleId: module.id.toString(),
    selector: 'backlog.component',
    templateUrl: 'backlog.component.html',
    styleUrls: ['backlog.component.css'],
})

export class BacklogComponent{
  currentUser: User;
  currentProject: Project;
  del: number;
  story: Story;
  name: string;
  epic_name: string;
  description: string;
  login: string;
  backlogList: Story[] = [];
  loading = false;
  currentBacklog: Sprint;


  constructor(
    public dialog: MatDialog, private router: Router, private storyService: StoryService, private alertService: AlertService) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.currentProject = JSON.parse(localStorage.getItem('currentProject'));
  }

  ngOnInit() {
    for( let sprint of this.currentProject.sprints){
      if(sprint.isBacklog){
        this.currentBacklog = sprint;
        break;
      }
    }

    this.loadAllStories(this.currentBacklog.id);
  }

  loadAllStories(id:number){
    this.storyService.loadAllStories(id).subscribe(stories => { this.backlogList = stories; });
  }

  openDialog(): void {
    let dialogRef = this.dialog.open(BacklogDialogComponent, {
      width: '400px',
      data: { name: this.name, description: this.description, login: this.login}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('Add story requested');

      this.name = result.name;
      this.description = result.description;


      this.story = new Story;
      this.story.name = this.name;
      this.story.description = this.description;
      this.story.sprint = this.currentBacklog;

      for( let user of this.currentProject.users){
        if(user.login === result.login){
          this.story.user = user;
          break;
        }
      }


      this.story.epic = result.epic;
      // this.epic_name = this.story.epic.name;
      // console.log(this.epic_name);
      // this.story.login = this.login;

      this.storyService.create(this.story)
          .subscribe(
              data => {
                  this.alertService.success('Story added', true);
              },
              error => {
                  this.alertService.error('Story creation failed', false);
              });

      this.backlogList.push(this.story);

      this.name = '';
      this.description = '';
      // this.login = '';
    });
  }

  openDeleteDialog(i): void {
    let dialogRef_delete = this.dialog.open(BacklogDeleteDialogComponent, {
      width: '400px'
    });

    dialogRef_delete.afterClosed().subscribe(result => {
      // console.log(result);
      if (result == false) {
        console.log("canceled");
      }
      else{
        console.log("removed");
        this.backlogList.splice(i, 1);
      }
    });
  }
}


@Component({
  moduleId: module.id.toString(),
  selector: 'backlog.dialog.component',
  templateUrl: 'backlog.dialog.component.html',
  styleUrls: ['backlog.component.css']
})

export class BacklogDialogComponent {
  users: User[] = [];
  currentProject: Project;
  epicsList : Epic[] = [];

  constructor(
    public dialogRef: MatDialogRef<BacklogDialogComponent>,
    private userService: UserService,
    private epicService: EpicService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.currentProject = JSON.parse(localStorage.getItem('currentProject'));
  }

  ngOnInit() {
    this.loadAllEpics();
  }

  onNoClick(): void {
    this.dialogRef.close();
    console.log('Add story canceled');
  }

  private loadAllEpics() {
      this.epicService.getAll(this.currentProject.id).subscribe(epicsList => { this.epicsList = epicsList; });
  }
  // private loadUsers() {
  //   this.userService.getProjectUsers().subscribe(users => { this.users = users; });
  // }
}

@Component({
  moduleId: module.id.toString(),
  selector: 'backlog.delete.dialog.component',
  templateUrl: 'backlog.delete.dialog.component.html',
  styleUrls: ['backlog.component.css']
})

export class BacklogDeleteDialogComponent {
  users: User[] = [];

  constructor(
    public dialogRef_delete: MatDialogRef<BacklogDeleteDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
  }
}
