import {Component,OnInit} from '@angular/core';
import { Router } from '@angular/router';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {MatChipsModule} from '@angular/material/chips';
import { User } from '../../_models/index';
import { AlertService, UserService} from '../../_services/index';
//import {FormControl, Validators} from '@angular/forms';


@Component({
  moduleId: module.id.toString(),
  selector: 'settings.component',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css'],
})

export class SettingsComponent implements OnInit {
  currentUser: User;
  users: User[] = [];

  constructor(private userService: UserService) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
   }
  ngOnInit() {
    //this.loadAllUsers();
  }
}
