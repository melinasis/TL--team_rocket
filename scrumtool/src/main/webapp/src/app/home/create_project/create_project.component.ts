﻿import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User, Sprint } from '../../_models/index';
import { AlertService, UserService, ProjectService, SprintService} from '../../_services/index';
import { FormControl } from '@angular/forms';
@Component({
    moduleId: module.id.toString(),
    templateUrl: './create_project.component.html',
    styleUrls: ['./create_project.component.css']
})

export class CreateProjectComponent implements OnInit {
    currentUser: User;
    data: any = {};
    loading = false;
    userList: User[] = [];
    users = new FormControl();

    constructor(private router: Router, private sprintService: SprintService, private userService: UserService, private projectService: ProjectService, private alertService: AlertService) {
    	this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }

    ngOnInit() {
        this.loadAllUsers();
    }

    private loadAllUsers() {
        this.userService.getAll().subscribe(users => { this.userList = users; });
    }

    cancel(): void {
      this.router.navigate(['']);
      console.log('Create project canceled');
    }


    createProject() {
      var newUsers: User[] = [];
      console.log(this.userList);
      for( let login of this.users.value){
        for( let user of this.userList){
          if(user.login === login){
            newUsers.push(user);
          }
        }
      }
      this.data['users'] = newUsers;

      console.log(this.data);
        this.loading = true;
        this.projectService.create(this.data)
            .subscribe(
                data => {
                  this.alertService.success('Creation of project successful', true);
                  this.router.navigate(['']);
                },
                error => {
            	    console.log(error);
                    this.alertService.error('Creation of project failed!', false);
                    this.loading = false;
                });
    }


}
