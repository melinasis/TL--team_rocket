import { Component, OnInit, Inject } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import { Router } from '@angular/router';
import {MatChipsModule} from '@angular/material/chips';
import { User, Project } from '../../_models/index';
import { UserService, ProjectService, AlertService} from '../../_services/index';


@Component({
  selector: module.id.toString(),
  templateUrl: './edit-project.component.html',
  styleUrls: ['./edit-project.component.css']
})

export class EditProjectComponent implements OnInit {
	currentUser: User;
	currentProject: Project;
	membersList = [];
  login: string;
  loading = false;

	constructor(private router: Router, private userService: UserService, private projectService: ProjectService, private alertService: AlertService, public dialog: MatDialog) {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.currentProject = JSON.parse(localStorage.getItem('currentProject'));
        this.membersList = this.currentProject.users;
        console.log(this.membersList);
    }

  	ngOnInit() {
  	}

    update(){
      console.log("update");
      this.loading = true;
          this.projectService.update(this.currentProject)
            .subscribe(
                data => {
                    this.alertService.success('Project updated successfully', true);
                    this.currentProject = data;
                    localStorage.setItem('currentProject', JSON.stringify(this.currentProject));
                    this.router.navigate(['./project']);
                },
                error => {
                  console.log(error);
                    this.alertService.error('Project failed to update!', false);
                    this.loading = false;
                });
    }

    delete(): void{
        console.log("delete");
         let dialogRef_delete = this.dialog.open(EProjectDeleteDialogComponent, {
         width: '400px'
        });

        dialogRef_delete.afterClosed().subscribe(result => {
        // console.log(result);
        if (result == false) {
          console.log("canceled");
        }
        else{
          console.log("removed");
          this.loading = true;
          this.projectService.delete(this.currentProject.id)
            .subscribe(
                () => {
                    this.alertService.success('Project deleted successfully', true);
                    this.router.navigate(['']);
                },
                error => {
                  console.log(error);
                    this.alertService.error('Project failed to delete!', false);
                    this.loading = false;
                });
        }
      });
    }

    openDialog(): void {
      let dialogRef = this.dialog.open(EProjectDialogComponent, {
        width: '400px',
        data: { login: this.login}
      });

      dialogRef.afterClosed().subscribe(result => {
        console.log('Add member requested');

        this.login = result.login;
        let flag = 0;
        for(let i=0; i<this.membersList.length; i++){
          if(this.login === this.membersList[i].login) flag=1;
        }
        if(flag === 0){
          this.userService.getByUsername(this.login)
            .subscribe(
                data => {
                    this.membersList.push(data);
                    this.currentProject.users = this.membersList;
                },
                error => {
                  console.log(error);
                    this.alertService.error('Failed to get user!', false);
                });
        }
        else{
          this.alertService.error('Member:'+ this.login +' has already been selected!', false);
          this.router.navigate(['./edit-project']);
        }

        this.login = '';
      });
    }

    removeItem(i): void {
      let dialogRef_delete = this.dialog.open(EProjectRemoveMemberDialogComponent, {
        width: '400px'
      });

      dialogRef_delete.afterClosed().subscribe(result => {
        if (result == false) {
          console.log("canceled");
        }
        else{
          console.log("removed");
          this.membersList.splice(i, 1);
        }
      });
     }
}

@Component({
  moduleId: module.id.toString(),
  selector: 'eproject.dialog.component',
  templateUrl: 'eproject.dialog.component.html',
  styleUrls: ['edit-project.component.css']
})

export class EProjectDialogComponent {
  users: User[] = [];
  roles = [
    {value: 'product-0', viewValue: 'Product Owner'},
    {value: 'scrum-1', viewValue: 'Scrum Master'},
    {value: 'dev-2', viewValue: 'Developer'}
  ];

  constructor(
    public dialogRef: MatDialogRef<EProjectDialogComponent>,
    private userService: UserService,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
      this.loadAllUsers();
  }

  onNoClick(): void {
    this.dialogRef.close();
    console.log('Add member cancelled');
  }

  private loadAllUsers() {
      this.userService.getAll().subscribe(users => { this.users = users; });
  }
}

@Component({
  moduleId: module.id.toString(),
  selector: 'eproject.delete.dialog.component',
  templateUrl: 'eproject.delete.dialog.component.html',
  styleUrls: ['edit-project.component.css']
})

export class EProjectDeleteDialogComponent {
  users: User[] = [];

  constructor(
    public dialogRef_delete: MatDialogRef<EProjectDeleteDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
  }
}

@Component({
  moduleId: module.id.toString(),
  selector: 'eproject.remove.member.dialog.component',
  templateUrl: 'eproject.remove.member.dialog.component.html',
  styleUrls: ['edit-project.component.css']
})

export class EProjectRemoveMemberDialogComponent {
  users: User[] = [];

  constructor(
    public dialogRef_delete: MatDialogRef<EProjectRemoveMemberDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
  }
}
