﻿import { Component, OnInit } from '@angular/core';

import { User } from '../../_models/index';
import { UserService } from '../../_services/index';

@Component({
    moduleId: module.id.toString(),
    templateUrl: 'team.component.html',
    styleUrls: ['./team.component.css']
})

export class TeamComponent implements OnInit {
    currentUser: User;
    users: User[] = [];

    constructor(private userService: UserService) {
    }

    ngOnInit() {
    }

}
