import {Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../../_models/index';
import { AlertService, UserService} from '../../_services/index';


@Component({
  moduleId: module.id.toString(),
  selector: 'profile.component',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
//export class ButtonTypesExample {}
 export class ProfileComponent implements OnInit {
   currentUser: User;
   users: User[] = [];

   constructor(private userService: UserService) {
     //this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }
   ngOnInit() {
     this.userService.getAccount().subscribe(user => { this.currentUser = user; });
   }
 }
