﻿import { Component, OnInit } from '@angular/core';

import { User, Project } from '../_models/index';
import { UserService, ProjectService} from '../_services/index';
import { Router } from '@angular/router';

@Component({
    moduleId: module.id.toString(),
    templateUrl: 'home.component.html',
    styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
    currentUser: User;
    users: User[] = [];
    projects: Project[] = [];
    project: any = {};

    constructor(private router: Router, private userService: UserService, private projectService: ProjectService) {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }

    ngOnInit() {
        this.loadAllProjects();
        //this.loadProjectsOfUser(this.currentUser.login);
    }

    deleteUser(id: number) {
        this.userService.delete(id).subscribe(() => { this.loadAllUsers() });
    }

    private loadAllUsers() {
        this.userService.getAll().subscribe(users => { this.users = users; });
    }

    private loadAllProjects() {
        this.projectService.getAll().subscribe(projects => { this.projects = projects; });
    }

    private loadProjectsOfUser(login: string) { //not functional yet
        this.userService.getByUsername(login).subscribe(projects => { this.project = projects; });
    }

    onClick(i){
        localStorage.setItem('currentProject', JSON.stringify(this.projects[i]));
    }

    navProject() {
      this.router.navigate(['/project']);
    }
}
