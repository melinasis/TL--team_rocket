﻿import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/index';
import { LoginComponent } from './user/login/index';
import { RegisterComponent } from './user/register/index';
import { AuthGuard } from './_guards/index';
import { UserComponent } from './user/user.component';
import { TeamComponent } from './home/team/index';
import { ProjectComponent } from './home/project/index';
import { CreateProjectComponent } from './home/create_project/index';
import { BacklogComponent } from './home/backlog/index';
import { SprintComponent } from './home/sprint/index';
import { EditProjectComponent } from './home/edit-project/index';
import { EpicsComponent } from './home/epics/index';
import { ProfileComponent } from './home/profile/index';
import { SettingsComponent } from './home/settings/index';

const appRoutes: Routes = [

  { path: '', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'team', component: TeamComponent, canActivate: [AuthGuard] },
  { path: 'project', component: ProjectComponent, canActivate: [AuthGuard] },
  { path: 'backlog', component: BacklogComponent, canActivate: [AuthGuard] },
  { path: 'create_project', component: CreateProjectComponent, canActivate: [AuthGuard] },
  { path: 'login', component: UserComponent,
    children: [{ path: '', component: LoginComponent }]},
  { path: 'register', component: UserComponent,
    children: [{ path: '', component: RegisterComponent }]},
  { path: 'sprint', component: SprintComponent, canActivate: [AuthGuard]},
  { path: 'edit-project', component: EditProjectComponent, canActivate: [AuthGuard]},
  { path: 'epics', component: EpicsComponent, canActivate: [AuthGuard]},
  { path: 'profile', component: ProfileComponent,canActivate: [AuthGuard]},
  { path: 'settings', component: SettingsComponent,canActivate: [AuthGuard]}
  // otherwise redirect to home
  //{ path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes, { useHash: true })
