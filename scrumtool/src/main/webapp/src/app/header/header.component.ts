import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User, Project } from '../_models/index';
import { UserService, ProjectService } from '../_services/index';

@Component({
	moduleId: module.id.toString(),
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})

export class HeaderComponent implements OnInit{
    currentUser: User
		projects: Project[] = [];

    constructor(private router: Router, private userService: UserService, private projectService: ProjectService) {
    	this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }

    ngOnInit() {
			this.loadAllProjects();
    }

		private loadAllProjects() {
			this.projectService.getAll().subscribe(projects => { this.projects = projects; });
		}

		onClick(i){
        localStorage.setItem('currentProject', JSON.stringify(this.projects[i]));
    }

		navHome(){
			this.router.navigate(['']);
		}
}
