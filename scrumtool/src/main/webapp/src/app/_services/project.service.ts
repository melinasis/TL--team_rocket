import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Project } from '../_models/index';

@Injectable()
export class ProjectService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<Project[]>('/api/projects');
    }

    getById(id: number) {
        return this.http.get('/api/projects/' + id);
    }

    create(project: Project) {
        return this.http.post('/api/projects', project);
    }

    update(project: Project) {
        return this.http.put<Project>('/api/projects/', project);
    }

    delete(id: number) {
        return this.http.delete('/api/projects/' + id);
    }
}
