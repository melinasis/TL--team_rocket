﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '../_models/index';

@Injectable()
export class UserService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<User[]>('/api/users');
    }

    getProjectUsers(){

    }

    getById(id: number) {
        return this.http.get('/api/users/' + id);
    }

    getByUsername(login: string) {
        return this.http.get<User>('/api/users/' + login);
    }

    create(user: User) {
        var register  = {
            'login': user.login,
            'password': user.password,
            'firstName': user.firstName,
            'lastName': user.firstName,
            'email': user.email
        };
        return this.http.post('/api/register', register);
    }

    update(user: User) {
        return this.http.put('/users/' + user.id, user);
    }

    delete(id: number) {
        return this.http.delete('/users/' + id);
    }

    getAccount(){
      return this.http.get<User>('/api/account');
    }
}
