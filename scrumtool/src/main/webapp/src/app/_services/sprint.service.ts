import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Sprint } from '../_models/index';

@Injectable()
export class SprintService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<Sprint[]>('/api/sprints');
    }

    getById(id: number) {
        return this.http.get('/api/sprints/' + id);
    }

    create(sprint: Sprint) {
        return this.http.post('/api/sprints', sprint);
    }

    update(sprint: Sprint) {
        return this.http.put('/api/sprints/' + sprint.id, sprint);
    }

    delete(id: number) {
        return this.http.delete('/api/sprints/' + id);
    }
}
