import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Epic } from '../_models/index';

@Injectable()
export class EpicService {
    constructor(private http: HttpClient) { }

    getAll(projectId: number) {
        return this.http.get<Epic[]>('/api/epics?projectId.equals=' + projectId);
    }

    getById(id: number) {
        return this.http.get('/api/epics/' + id);
    }

    create(epic: Epic) {
        return this.http.post('/api/epics', epic);
    }

    update(epic: Epic) {
        return this.http.put('/api/epics/' + epic.id, epic);
    }

    delete(id: number) {
        return this.http.delete('/api/epic/' + id);
    }
}
