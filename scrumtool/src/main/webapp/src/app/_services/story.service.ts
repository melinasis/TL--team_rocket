import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Story } from '../_models/index';

@Injectable()
export class StoryService {
    constructor(private http: HttpClient) { }

    create(story: Story) {
        return this.http.post('/api/stories', story);
    }

    loadAllStories(id: number) {
      return this.http.get<Story[]>( '/api/stories?sprintId.equals=' + id);
    }
}
