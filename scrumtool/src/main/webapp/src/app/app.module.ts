﻿import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent }  from './app.component';
import { routing } from './app.routing';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';

import { AlertComponent } from './_directives/index';
import { AuthGuard } from './_guards/index';
import { JwtInterceptor } from './_helpers/index';
import { AlertService, AuthenticationService, UserService, ProjectService, StoryService, EpicService, SprintService} from './_services/index';
import { HomeComponent } from './home/index';
import { TeamComponent } from './home/team/index';
import { ProjectComponent } from './home/project/index';
import { CreateProjectComponent } from './home/create_project/index';
import { HeaderComponent } from './header/index';
import { LoginComponent } from './user/login/index';
import { RegisterComponent } from './user/register/index';
import { UserComponent } from './user/user.component';
import { BacklogComponent } from './home/backlog/index';
import { BacklogDialogComponent } from './home/backlog/index';
import { BacklogDeleteDialogComponent } from './home/backlog/index';
import { SprintComponent } from './home/sprint/index';
import { EditProjectComponent } from './home/edit-project/index';
import { EProjectDialogComponent } from './home/edit-project/index';
import { EProjectDeleteDialogComponent } from './home/edit-project/index';
import { EProjectRemoveMemberDialogComponent } from './home/edit-project/index';
import { EpicsComponent } from './home/epics/index';
import { EpicsDialogComponent } from './home/epics/index';
import { EpicsDeleteDialogComponent } from './home/epics/index';
import { ProfileComponent } from './home/profile/index';
import { SettingsComponent } from './home/settings/index';
import { SidemenuProject } from './sidemenu-project/index';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        routing,
        BrowserAnimationsModule,
        MaterialModule,
        ReactiveFormsModule
    ],
    declarations: [
        AppComponent,
        AlertComponent,
        HomeComponent,
        LoginComponent,
        RegisterComponent,
        UserComponent,
        TeamComponent,
        ProjectComponent,
        CreateProjectComponent,
        HeaderComponent,
        SprintComponent,
        BacklogComponent,
        BacklogDialogComponent,
        BacklogDeleteDialogComponent,
        EditProjectComponent,
        EProjectDialogComponent,
        EProjectDeleteDialogComponent,
        EProjectRemoveMemberDialogComponent,
        EpicsComponent,
        EpicsDialogComponent,
        EpicsDeleteDialogComponent,
        ProfileComponent,
        SettingsComponent,
        SidemenuProject
    ],
    providers: [
        AuthGuard,
        AlertService,
        AuthenticationService,
        UserService,
        ProjectService,
        StoryService,
        SprintService,
        EpicService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
        },

    ],
    entryComponents: [EpicsDeleteDialogComponent, BacklogDeleteDialogComponent, BacklogDialogComponent, EProjectDialogComponent, EProjectDeleteDialogComponent, EProjectRemoveMemberDialogComponent,EpicsDialogComponent],
    bootstrap: [AppComponent]
})

export class AppModule { }
