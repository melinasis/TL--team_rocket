import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'sidemenu-project',
  templateUrl: './sidemenu-project.component.html',
  styleUrls: ['./sidemenu-project.component.css']
})
export class SidemenuProject implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  navBacklog() {
    this.router.navigate(['/backlog']);
  }

  navSprint() {
    this.router.navigate(['/sprint']);
  }

  navProject() {
    this.router.navigate(['/project']);
  }

  navEpics() {
    this.router.navigate(['/epics']);
  }

  navEditProject()  {
    this.router.navigate(['/edit-project']);
  }
}
