import { Sprint, User, Epic } from './index';

export class Story {
    id: number;
    name: string;
    description: string;
    sprint: Sprint;
    user: User;
    epic: Epic;
}
