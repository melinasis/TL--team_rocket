import { Story, Project } from './index';

export class Sprint {
    id: number;
    name: string;
    startDate: Date;
    endDate: Date;
    isBacklog: boolean;
    project:Project;
    stories:Array<Story>;

}
