import { User, Sprint } from './index';

export class Project {
    id: number;
    name: string;
    description: string;
    users: Array<User>;
    sprints: Array<Sprint>;
}
