﻿export class User {
    id: number;
    login: string;
    password: string;
    firstName: string;
    email: string;
    token: string;
}
