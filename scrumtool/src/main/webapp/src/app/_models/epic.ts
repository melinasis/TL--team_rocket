import { Project } from './index';

export class Epic {
    id: number;
    name: string;
    description: string;
    project: Project;
}
