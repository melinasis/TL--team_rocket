package scrumtool.web.rest;

import scrumtool.domain.Epic;
import scrumtool.service.EpicService;
import scrumtool.web.rest.errors.BadRequestAlertException;
import scrumtool.web.rest.util.HeaderUtil;
import scrumtool.service.dto.EpicCriteria;
import scrumtool.service.EpicQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Epic.
 */
@RestController
@RequestMapping("/api")
public class EpicResource {

    private final Logger log = LoggerFactory.getLogger(EpicResource.class);

    private static final String ENTITY_NAME = "epic";

    private final EpicService epicService;

    private final EpicQueryService epicQueryService;

    public EpicResource(EpicService epicService, EpicQueryService epicQueryService) {
        this.epicService = epicService;
        this.epicQueryService = epicQueryService;
    }

    /**
     * POST  /epics : Create a new epic.
     *
     * @param epic the epic to create
     * @return the ResponseEntity with status 201 (Created) and with body the new epic, or with status 400 (Bad Request) if the epic has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/epics")
    public ResponseEntity<Epic> createEpic(@Valid @RequestBody Epic epic) throws URISyntaxException {
        log.debug("REST request to save Epic : {}", epic);
        if (epic.getId() != null) {
            throw new BadRequestAlertException("A new epic cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Epic result = epicService.save(epic);
        return ResponseEntity.created(new URI("/api/epics/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /epics : Updates an existing epic.
     *
     * @param epic the epic to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated epic,
     * or with status 400 (Bad Request) if the epic is not valid,
     * or with status 500 (Internal Server Error) if the epic couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/epics")
    public ResponseEntity<Epic> updateEpic(@Valid @RequestBody Epic epic) throws URISyntaxException {
        log.debug("REST request to update Epic : {}", epic);
        if (epic.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Epic result = epicService.save(epic);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, epic.getId().toString()))
            .body(result);
    }

    /**
     * GET  /epics : get all the epics.
     *
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of epics in body
     */
    @GetMapping("/epics")
    public ResponseEntity<List<Epic>> getAllEpics(EpicCriteria criteria) {
        log.debug("REST request to get Epics by criteria: {}", criteria);
        List<Epic> entityList = epicQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
     * GET  /epics/:id : get the "id" epic.
     *
     * @param id the id of the epic to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the epic, or with status 404 (Not Found)
     */
    @GetMapping("/epics/{id}")
    public ResponseEntity<Epic> getEpic(@PathVariable Long id) {
        log.debug("REST request to get Epic : {}", id);
        Optional<Epic> epic = epicService.findOne(id);
        return ResponseUtil.wrapOrNotFound(epic);
    }

    /**
     * DELETE  /epics/:id : delete the "id" epic.
     *
     * @param id the id of the epic to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/epics/{id}")
    public ResponseEntity<Void> deleteEpic(@PathVariable Long id) {
        log.debug("REST request to delete Epic : {}", id);
        epicService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
