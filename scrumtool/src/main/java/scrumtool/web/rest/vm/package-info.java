/**
 * View Models used by Spring MVC REST controllers.
 */
package scrumtool.web.rest.vm;
