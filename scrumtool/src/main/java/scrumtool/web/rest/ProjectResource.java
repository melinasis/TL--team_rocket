package scrumtool.web.rest;

import scrumtool.domain.Project;
import scrumtool.domain.User;
import scrumtool.domain.Sprint;
import scrumtool.service.ProjectService;
import scrumtool.service.SprintService;
import scrumtool.web.rest.errors.BadRequestAlertException;
import scrumtool.web.rest.util.HeaderUtil;
import scrumtool.service.dto.ProjectCriteria;
import scrumtool.service.ProjectQueryService;
import scrumtool.security.SecurityUtils;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * REST controller for managing Project.
 */
@RestController
@RequestMapping("/api")
public class ProjectResource {

    private final Logger log = LoggerFactory.getLogger(ProjectResource.class);

    private static final String ENTITY_NAME = "project";

    private final ProjectService projectService;
    private final SprintService sprintService;

    private final ProjectQueryService projectQueryService;

    public ProjectResource(ProjectService projectService, ProjectQueryService projectQueryService, SprintService sprintService) {
        this.projectService = projectService;
        this.projectQueryService = projectQueryService;
        this.sprintService = sprintService;
    }

    /**
     * POST  /projects : Create a new project.
     *
     * @param project the project to create
     * @return the ResponseEntity with status 201 (Created) and with body the new project, or with status 400 (Bad Request) if the project has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/projects")
    public ResponseEntity<Project> createProject(@Valid @RequestBody Project project) throws URISyntaxException {
        log.debug("REST request to save Project : {}", project);
        if (project.getId() != null) {
            throw new BadRequestAlertException("A new project cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Project proj = projectService.save(project);
        Sprint backlog = new Sprint();
        backlog.setName("Backlog");
        backlog.setIsBacklog(true);
        backlog.setProject(proj);
        sprintService.save(backlog);
        Project result = projectService.save(proj);
        return ResponseEntity.created(new URI("/api/projects/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /projects : Updates an existing project.
     *
     * @param project the project to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated project,
     * or with status 400 (Bad Request) if the project is not valid,
     * or with status 500 (Internal Server Error) if the project couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/projects")
    public ResponseEntity<Project> updateProject(@Valid @RequestBody Project project) throws URISyntaxException {
        log.debug("REST request to update Project : {}", project);
        if (project.getId() == null || !projectService.findOne(project.getId()).isPresent()) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Project result = projectService.save(project);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, project.getId().toString()))
            .body(result);
    }

    /**
     * GET  /projects : get all the projects.
     *
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of projects in body
     */
    @GetMapping("/projects")
    public ResponseEntity<List<Project>> getAllProjects(ProjectCriteria criteria) {
        log.debug("REST request to get Projects by criteria: {}", criteria);
        List<Project> entityList = projectQueryService.findByCriteria(criteria);
        List<Project> respList = new ArrayList<Project>();
        String login = SecurityUtils.getCurrentUserLogin().orElse("");
        for (Project p: entityList) {
            Set<User> userSet = p.getUsers();
            for (User u: userSet) {
                if (u.getLogin().equals(login)) {
                    respList.add(p);
                    break;
                }
            }

        }
        return ResponseEntity.ok().body(respList);
    }

    /**
     * GET  /projects/:id : get the "id" project.
     *
     * @param id the id of the project to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the project, or with status 404 (Not Found)
     */
    @GetMapping("/projects/{id}")
    public ResponseEntity<Project> getProject(@PathVariable Long id) {
        log.debug("REST request to get Project : {}", id);
        Optional<Project> project = projectService.findOne(id);
        return ResponseUtil.wrapOrNotFound(project);
    }

    /**
     * DELETE  /projects/:id : delete the "id" project.
     *
     * @param id the id of the project to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/projects/{id}")
    public ResponseEntity<Void> deleteProject(@PathVariable Long id) {
        log.debug("REST request to delete Project : {}", id);
        Optional<Project> project = projectService.findOne(id);
        if (project.isPresent()) {
            Project proj = project.get();
            Set<Sprint> sprintSet = proj.getSprints();
            for (Sprint s: sprintSet) {
                sprintService.delete(s.getId());
            }
            projectService.delete(id);

            return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
        }
        return ResponseEntity.notFound().build();
    }
}
