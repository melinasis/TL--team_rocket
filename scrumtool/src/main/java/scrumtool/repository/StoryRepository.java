package scrumtool.repository;

import scrumtool.domain.Story;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Story entity.
 */
@SuppressWarnings("unused")
@Repository
public interface StoryRepository extends JpaRepository<Story, Long>, JpaSpecificationExecutor<Story> {

    @Query("select story from Story story where story.user.login = ?#{principal.username}")
    List<Story> findByUserIsCurrentUser();

    @Query(value = "select distinct story from Story story left join fetch story.epics",
        countQuery = "select count(distinct story) from Story story")
    Page<Story> findAllWithEagerRelationships(Pageable pageable);

    @Query(value = "select distinct story from Story story left join fetch story.epics")
    List<Story> findAllWithEagerRelationships();

    @Query("select story from Story story left join fetch story.epics where story.id =:id")
    Optional<Story> findOneWithEagerRelationships(@Param("id") Long id);

}
