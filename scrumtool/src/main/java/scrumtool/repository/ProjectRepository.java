package scrumtool.repository;

import scrumtool.domain.Project;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Project entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProjectRepository extends JpaRepository<Project, Long>, JpaSpecificationExecutor<Project> {

    @Query(value = "select distinct project from Project project left join fetch project.users",
        countQuery = "select count(distinct project) from Project project")
    Page<Project> findAllWithEagerRelationships(Pageable pageable);

    @Query(value = "select distinct project from Project project left join fetch project.users")
    List<Project> findAllWithEagerRelationships();

    @Query("select project from Project project left join fetch project.users where project.id =:id")
    Optional<Project> findOneWithEagerRelationships(@Param("id") Long id);

    @Query("select project from Project project left join fetch project.users as u where project.id =:id and u.login =:login")
    Optional<Project> findOneWithEagerRelationships(@Param("id") Long id, @Param("login") String login);
}
