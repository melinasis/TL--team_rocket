package scrumtool.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import scrumtool.domain.Story;
import scrumtool.domain.*; // for static metamodels
import scrumtool.repository.StoryRepository;
import scrumtool.service.dto.StoryCriteria;


/**
 * Service for executing complex queries for Story entities in the database.
 * The main input is a {@link StoryCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Story} or a {@link Page} of {@link Story} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class StoryQueryService extends QueryService<Story> {

    private final Logger log = LoggerFactory.getLogger(StoryQueryService.class);

    private final StoryRepository storyRepository;

    public StoryQueryService(StoryRepository storyRepository) {
        this.storyRepository = storyRepository;
    }

    /**
     * Return a {@link List} of {@link Story} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Story> findByCriteria(StoryCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Story> specification = createSpecification(criteria);
        return storyRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Story} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Story> findByCriteria(StoryCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Story> specification = createSpecification(criteria);
        return storyRepository.findAll(specification, page);
    }

    /**
     * Function to convert StoryCriteria to a {@link Specification}
     */
    private Specification<Story> createSpecification(StoryCriteria criteria) {
        Specification<Story> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Story_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Story_.name));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), Story_.description));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildSpecification(criteria.getStatus(), Story_.status));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getUserId(), Story_.user, User_.id));
            }
            if (criteria.getEpicsId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getEpicsId(), Story_.epics, Epic_.id));
            }
            if (criteria.getSprintId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getSprintId(), Story_.sprint, Sprint_.id));
            }
        }
        return specification;
    }

}
