package scrumtool.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import scrumtool.domain.Epic;
import scrumtool.domain.*; // for static metamodels
import scrumtool.repository.EpicRepository;
import scrumtool.service.dto.EpicCriteria;


/**
 * Service for executing complex queries for Epic entities in the database.
 * The main input is a {@link EpicCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Epic} or a {@link Page} of {@link Epic} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EpicQueryService extends QueryService<Epic> {

    private final Logger log = LoggerFactory.getLogger(EpicQueryService.class);

    private final EpicRepository epicRepository;

    public EpicQueryService(EpicRepository epicRepository) {
        this.epicRepository = epicRepository;
    }

    /**
     * Return a {@link List} of {@link Epic} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Epic> findByCriteria(EpicCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Epic> specification = createSpecification(criteria);
        return epicRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Epic} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Epic> findByCriteria(EpicCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Epic> specification = createSpecification(criteria);
        return epicRepository.findAll(specification, page);
    }

    /**
     * Function to convert EpicCriteria to a {@link Specification}
     */
    private Specification<Epic> createSpecification(EpicCriteria criteria) {
        Specification<Epic> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Epic_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Epic_.name));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), Epic_.description));
            }
            if (criteria.getProjectId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getProjectId(), Epic_.project, Project_.id));
            }
            if (criteria.getStoriesId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getStoriesId(), Epic_.stories, Story_.id));
            }
        }
        return specification;
    }

}
