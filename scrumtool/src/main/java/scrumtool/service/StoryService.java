package scrumtool.service;

import scrumtool.domain.Story;
import scrumtool.repository.StoryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;
import java.util.Optional;
/**
 * Service Implementation for managing Story.
 */
@Service
@Transactional
public class StoryService {

    private final Logger log = LoggerFactory.getLogger(StoryService.class);

    private final StoryRepository storyRepository;

    public StoryService(StoryRepository storyRepository) {
        this.storyRepository = storyRepository;
    }

    /**
     * Save a story.
     *
     * @param story the entity to save
     * @return the persisted entity
     */
    public Story save(Story story) {
        log.debug("Request to save Story : {}", story);        return storyRepository.save(story);
    }

    /**
     * Get all the stories.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<Story> findAll() {
        log.debug("Request to get all Stories");
        return storyRepository.findAllWithEagerRelationships();
    }

    /**
     * Get all the Story with eager load of many-to-many relationships.
     *
     * @return the list of entities
     */
    public Page<Story> findAllWithEagerRelationships(Pageable pageable) {
        return storyRepository.findAllWithEagerRelationships(pageable);
    }
    

    /**
     * Get one story by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<Story> findOne(Long id) {
        log.debug("Request to get Story : {}", id);
        return storyRepository.findOneWithEagerRelationships(id);
    }

    /**
     * Delete the story by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Story : {}", id);
        storyRepository.deleteById(id);
    }
}
