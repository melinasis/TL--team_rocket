package scrumtool.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import scrumtool.domain.Sprint;
import scrumtool.domain.*; // for static metamodels
import scrumtool.repository.SprintRepository;
import scrumtool.service.dto.SprintCriteria;


/**
 * Service for executing complex queries for Sprint entities in the database.
 * The main input is a {@link SprintCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Sprint} or a {@link Page} of {@link Sprint} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class SprintQueryService extends QueryService<Sprint> {

    private final Logger log = LoggerFactory.getLogger(SprintQueryService.class);

    private final SprintRepository sprintRepository;

    public SprintQueryService(SprintRepository sprintRepository) {
        this.sprintRepository = sprintRepository;
    }

    /**
     * Return a {@link List} of {@link Sprint} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Sprint> findByCriteria(SprintCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Sprint> specification = createSpecification(criteria);
        return sprintRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Sprint} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Sprint> findByCriteria(SprintCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Sprint> specification = createSpecification(criteria);
        return sprintRepository.findAll(specification, page);
    }

    /**
     * Function to convert SprintCriteria to a {@link Specification}
     */
    private Specification<Sprint> createSpecification(SprintCriteria criteria) {
        Specification<Sprint> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Sprint_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Sprint_.name));
            }
            if (criteria.getStartDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStartDate(), Sprint_.startDate));
            }
            if (criteria.getEndDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getEndDate(), Sprint_.endDate));
            }
            if (criteria.getIsBacklog() != null) {
                specification = specification.and(buildSpecification(criteria.getIsBacklog(), Sprint_.isBacklog));
            }
            if (criteria.getStoriesId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getStoriesId(), Sprint_.stories, Story_.id));
            }
            if (criteria.getProjectId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getProjectId(), Sprint_.project, Project_.id));
            }
        }
        return specification;
    }

}
