package scrumtool.service;

import scrumtool.domain.Epic;
import scrumtool.repository.EpicRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;
import java.util.Optional;
/**
 * Service Implementation for managing Epic.
 */
@Service
@Transactional
public class EpicService {

    private final Logger log = LoggerFactory.getLogger(EpicService.class);

    private final EpicRepository epicRepository;

    public EpicService(EpicRepository epicRepository) {
        this.epicRepository = epicRepository;
    }

    /**
     * Save a epic.
     *
     * @param epic the entity to save
     * @return the persisted entity
     */
    public Epic save(Epic epic) {
        log.debug("Request to save Epic : {}", epic);        return epicRepository.save(epic);
    }

    /**
     * Get all the epics.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<Epic> findAll() {
        log.debug("Request to get all Epics");
        return epicRepository.findAll();
    }


    /**
     * Get one epic by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<Epic> findOne(Long id) {
        log.debug("Request to get Epic : {}", id);
        return epicRepository.findById(id);
    }

    /**
     * Delete the epic by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Epic : {}", id);
        epicRepository.deleteById(id);
    }
}
